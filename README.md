A console application that allows a user to manage information for participants of a archery competition.

A database is generated using Entity Framework that contains 5 classes:

Competitor: contains information about the competitor.

Coach: has a one to one relationship to a Competitor. A Competitor has one Coach.

Team: has a one to many relationship to Competitors. Many Competitors belong to one Team.

Skill: has a many to many relationship to Competitors. Many Competitors can have many skills and vice versa. 

CompetitorSkill: a joining class between Competitor and Skills to handle the many to many relationship.


The OnConfiguring method override in the CompetitorDbContext class contains the connection string for the SQL database. 

The main method contains methods for seeding and deleting the contents of the database and a method for displaying the contents in the console. 