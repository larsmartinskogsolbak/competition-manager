﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Linq;

namespace Competition_Manager
{
    class Program
    {
        static void Main(string[] args)
        {
            //seedDatabase();
            showCompetitors();
            //deleteAllfromDB(); 
            //deleteCompetitors();

        }

        public static void seedDatabase()
        {
            using (CompetitorDBContext competitorDBContext = new CompetitorDBContext())
            {
                competitorDBContext.Database.EnsureCreated();
                Team t1 = new Team() { TeamName = "Sharpshooters" };
                Team t2 = new Team() { TeamName = "Strongbow" };

                Coach co1 = new Coach() { FirstName = "Erik", LastName = "Franklin" };
                Coach co2 = new Coach() { FirstName = "Alvin", LastName = "Ortiz" };
                Coach co3 = new Coach() { FirstName = "Lucas", LastName = "Frazier", CoachId = 3 };
                Coach co4 = new Coach() { FirstName = "Lucia", LastName = "Luna", CoachId = 4 };

                Competitor c1 = new Competitor() { FirstName = "Lars-Martin", LastName = "Skog Solbak", Age = 27, Coach = co1 };
                Competitor c2 = new Competitor() { FirstName = "Imaad", LastName = "Kirkland", Age = 20, Coach = co2 };
                Competitor c3 = new Competitor() { FirstName = "Jannah", LastName = "Hull", Age = 21, Coach = co4 };
                Competitor c4 = new Competitor() { FirstName = "Everly", LastName = "Hastings", Age = 23, Coach = co3 };
                Competitor c5 = new Competitor() { FirstName = "Jose", LastName = "Fuller", Age = 22 };

                Skill s1 = new Skill() { SkillName = "Precision" };
                Skill s2 = new Skill() { SkillName = "Consistency" };

                CompetitorSkill cs1 = new CompetitorSkill(c1, s1);
                CompetitorSkill cs2 = new CompetitorSkill(c1, s2);
                CompetitorSkill cs3 = new CompetitorSkill(c2, s1);
                CompetitorSkill cs4 = new CompetitorSkill(c2, s2);
                CompetitorSkill cs5 = new CompetitorSkill(c3, s1);
                CompetitorSkill cs6 = new CompetitorSkill(c4, s2);

                t1.competitors.Add(c1);
                t1.competitors.Add(c2);
                t1.competitors.Add(c3);
                t2.competitors.Add(c4);
                t2.competitors.Add(c5);

                competitorDBContext.Add(cs1);
                competitorDBContext.Add(cs2);
                competitorDBContext.Add(cs3);
                competitorDBContext.Add(cs4);
                competitorDBContext.Add(cs5);
                competitorDBContext.Add(cs6);

                competitorDBContext.Add(t1);
                competitorDBContext.Add(t2);

                competitorDBContext.SaveChanges();
            }
        }

        public static void showCompetitors()
        {
            using (CompetitorDBContext competitorDBContext = new CompetitorDBContext())
            {

                var comps = competitorDBContext.Competitors
                    .Include(c => c.Coach)
                    .Include(c => c.Team)
                    .Include(c => c.CompetitorSkills)
                    .ToList();

                var skills = competitorDBContext.Skills
                    .ToList();


                foreach (var competitor in comps)
                {
                    Console.WriteLine("Name: " + competitor.FirstName + " " + competitor.LastName + Environment.NewLine + "Age: " + competitor.Age);
                    if (competitor.Coach != null)
                    {
                        Console.WriteLine("Coach: " + competitor.Coach.FirstName + " " + competitor.Coach.LastName);
                    }
                    
                    Console.WriteLine("Team: " + competitor.Team.TeamName);
                    Console.WriteLine("Skills: ");

                    foreach(CompetitorSkill competitorSkill in competitor.CompetitorSkills)
                    {
                        Console.WriteLine(competitorSkill.Skill.SkillName);
                    }
                    Console.WriteLine(Environment.NewLine);
                }
                
            }
        }
        public static void deleteCompetitors()
        {
            using (CompetitorDBContext competitorDBContext = new CompetitorDBContext())
            {
                var competitors = competitorDBContext.Competitors.Where(s => s.Id > 13);
                competitorDBContext.Competitors.RemoveRange(competitors);
                
                competitorDBContext.SaveChanges();
            }
        }

        public static void deleteAllfromDB()
        {
            using (CompetitorDBContext competitorDBContext = new CompetitorDBContext())
            {
                foreach(Team team in competitorDBContext.Teams)
                {
                    competitorDBContext.Remove(team);
                }
                foreach (Skill skill in competitorDBContext.Skills)
                {
                    competitorDBContext.Remove(skill);
                }

                competitorDBContext.SaveChanges();
            }
        }
    }
}
