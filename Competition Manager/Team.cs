﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Competition_Manager { 
    public class Team
    {
        public int TeamId { get; set; }
        public string TeamName { get; set; }
        public List<Competitor> competitors { get; set; }
         
        public Team()
        {
            competitors = new List<Competitor>();
        }
        public Team(string teamName)
        {
            competitors = new List<Competitor>();
            TeamName = teamName;
        }
        
    }
}
