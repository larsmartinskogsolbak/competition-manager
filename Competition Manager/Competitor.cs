﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace Competition_Manager
{
    public class Competitor
    {
        public int Id { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public int Age { get; set; }
        
        [ForeignKey("Team")]
        public int TeamId { get; set; }

        public Team Team { get; set; }

        public virtual Coach Coach { get; set; }
        public List<CompetitorSkill> CompetitorSkills { get; set; }

        public Competitor()
        {
            CompetitorSkills = new List<CompetitorSkill>();
        }
        public Competitor(string firstName, string lastName, int age, Coach coach)
        {
            CompetitorSkills = new List<CompetitorSkill>();
            FirstName = firstName;
            LastName = lastName;
            Age = age;
            Coach = coach;
        }
        public Competitor(int id, string firstName, string lastName, int age)
        {
            Id = id;
            FirstName = firstName;
            LastName = lastName;
            Age = age;
        }

      
       
    }
}
