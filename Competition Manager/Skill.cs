﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Competition_Manager
{
    public class Skill
    {

        public Skill()
        {
            CompetitorSkills = new List<CompetitorSkill>();
            this.SkillName = "";
        }

        public Skill(string name)
        {
            CompetitorSkills = new List<CompetitorSkill>();
            this.SkillName = name;
        }
        public int SkillId { get; set;  } 
        public string SkillName { get; set; }

        public List<CompetitorSkill> CompetitorSkills { get; set; }
    

        
    }
}
