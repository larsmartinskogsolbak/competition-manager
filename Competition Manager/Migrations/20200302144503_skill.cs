﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace Competition_Manager.Migrations
{
    public partial class skill : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Competitors_Teams_TeamId",
                table: "Competitors");

            migrationBuilder.AlterColumn<int>(
                name: "TeamId",
                table: "Competitors",
                nullable: true,
                oldClrType: typeof(int),
                oldType: "int");

            migrationBuilder.AddForeignKey(
                name: "FK_Competitors_Teams_TeamId",
                table: "Competitors",
                column: "TeamId",
                principalTable: "Teams",
                principalColumn: "TeamId",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Competitors_Teams_TeamId",
                table: "Competitors");

            migrationBuilder.AlterColumn<int>(
                name: "TeamId",
                table: "Competitors",
                type: "int",
                nullable: false,
                oldClrType: typeof(int),
                oldNullable: true);

            migrationBuilder.AddForeignKey(
                name: "FK_Competitors_Teams_TeamId",
                table: "Competitors",
                column: "TeamId",
                principalTable: "Teams",
                principalColumn: "TeamId",
                onDelete: ReferentialAction.Cascade);
        }
    }
}
