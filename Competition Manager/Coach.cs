﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;

namespace Competition_Manager { 
    public class Coach
    {
       
        [ForeignKey("Competitor")]
        public int CoachId { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }

        public virtual Competitor Competitor { get; set; }

        public Coach()
        {

        }
        public Coach(string firstName, string lastName)
        {
            
            FirstName = firstName;
            LastName = lastName;
            
        }
    }
}
