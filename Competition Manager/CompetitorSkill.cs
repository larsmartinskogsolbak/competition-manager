﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Competition_Manager { 
    public class CompetitorSkill
    {
        public CompetitorSkill()
        {

        }
        public CompetitorSkill(Competitor competitor, Skill skill)
        {
            this.Competitor = competitor;
            this.Skill = skill;
        }
        [Key, Column(Order = 1)]
        public int CompetitorId { get; set; }
        [Key, Column(Order = 2)]
        public int SkillId { get; set; }
        public Skill Skill { get; set; }
        public Competitor Competitor { get; set; }
    }
}
